import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Product} from "../../../models/product.model";

@Component({
  selector: 'app-card-product',
  templateUrl: './card-product.component.html',
  styleUrls: ['./card-product.component.css']
})
export class CardProductComponent {


  @Input()
  product: Product;

  @Output() emitProduct = new EventEmitter;



  constructor( ) {
  }

  viewDetails(): void {
    this.emitProduct.emit(this.product);

  }

}
