import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ProductsComponent} from "./products.component";
import {MatCardModule} from "@angular/material/card";
import {MatTabsModule} from "@angular/material/tabs";
import {CardProductComponent} from "./card-product/card-product.component";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatPaginatorModule} from "@angular/material/paginator";
import {ProductService} from "../../core/product.service";
import {MatDialogModule} from "@angular/material/dialog";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {ViewDialogComponent} from "./view-dialog/view-dialog.component";
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {ProductsEffects} from "./product.effects";
import {productReducer} from "./product.reducer";


export const productRoutes: Routes = [
  {
    path: '',
    component: ProductsComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(productRoutes),
    MatCardModule,
    MatTabsModule,
    MatIconModule,
    MatButtonModule,
    MatPaginatorModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    StoreModule.forFeature('products', productReducer),
    EffectsModule.forFeature([
      ProductsEffects,
    ]),
  ],
  declarations: [
    ProductsComponent,
    CardProductComponent,
    ViewDialogComponent
  ],
  exports: [

  ],
  providers: [
    ProductService
  ],

})
export class ProductsModule {

  constructor() {

  }


}
