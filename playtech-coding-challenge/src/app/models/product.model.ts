
export enum TypeProduct {
  Sw, Mobile, Fashion
}

export class Product {
  id: number;
  name: string;
  iconUrl: string;
  description: string;
  longDescription: string;
  price: number;
  type: TypeProduct


  protected constructor(id: number, name: string, iconUrl: string, description: string, longDescription: string, price: number, type: TypeProduct) {
    this.id = id;
    this.name = name;
    this.iconUrl = iconUrl;
    this.description = description;
    this.longDescription = longDescription;
    this.price = price;
    this.type = type;
  }
}

export class Software extends Product {
  requirements: string;

  constructor(id: number,name: string, iconUrl: string, description: string, longDescription: string, price: number,type: TypeProduct, requirements: string) {
    super(id,name, iconUrl, description, longDescription, price, type);
    this.requirements = requirements;
  }
}

export class Fashion extends Product {
  color: string;
  size: string;
  constructor(id: number,name: string, iconUrl: string, description: string, longDescription: string, price: number, type: TypeProduct, color: string, size: string) {
    super(id,name, iconUrl, description, longDescription, price, type);
    this.color = color;
    this.size = size;
  }
}

export class MobileDevices extends Product {
  battery: string;
  processor: string;
  version: string;
  constructor(id: number,name: string, iconUrl: string, description: string, longDescription: string, price: number, type: TypeProduct, battery: string, processor: string, version: string) {
    super(id,name, iconUrl, description, longDescription, price, type);
    this.battery = battery;
    this.processor = processor;
    this.version = version;
  }
}
