import {createAction, props} from "@ngrx/store";
import {Product} from "../../models/product.model";


export const loadProducts = createAction('[Products Data] Load Products', props<{page: number}>());
export const loadUsersSuccess = createAction('[Products Data] Load Products Success', props<{products: Product[]}>());
