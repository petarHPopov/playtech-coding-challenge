import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {ProductService} from "../../core/product.service";
import {Product} from "../../models/product.model";
import {MatDialog} from "@angular/material/dialog";
import {ViewDialogComponent} from "./view-dialog/view-dialog.component";
import {Store} from "@ngrx/store";
import {loadProducts} from "./product.actions";
import {getProducts} from "./product.selectors";

@Component({
  selector: 'app-product',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, OnDestroy {

  productObs: Observable<any>;

  viewProductData: any;

  pageEvent: PageEvent | undefined;
  pageIndex: number = 0;
  pageSize: number = 2;
  length = 0;


  paginator: MatPaginator;
  dataSource: MatTableDataSource<Product> = new MatTableDataSource();


  constructor(private changeDetectorRef: ChangeDetectorRef,
              private service: ProductService,
              private dialog: MatDialog,
              private store: Store) {
  }

  ngAfterContentInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.changeDetectorRef?.detectChanges();
    this.productObs = this.dataSource.connect();

    this.getProducts();

  }

  getProducts(): void {
    this.store.dispatch(loadProducts({page: this.pageIndex + 1}));

    this.store.select(getProducts).subscribe(data => {
      this.dataSource.data = data;
      this.length = 5;
    });

  }

  ngOnDestroy() {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }

  viewProduct(evt: Product): void {

    this.service.getProduct(evt.id).subscribe(data => {

      this.dialog.open(ViewDialogComponent, {
        data
      });
    });

  }

  getServerData(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.getProducts();
    return event;
  }

}
