import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'dialog-test',
  templateUrl: 'view-dialog.component.html',
  styleUrls: ['view-dialog.component.css']
})
export class ViewDialogComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<ViewDialogComponent>) {
  }
}
