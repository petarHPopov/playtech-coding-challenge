import {Injectable} from "@angular/core";
import { Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import { Product} from "../models/product.model";

@Injectable()
export class ProductService {

  constructor(private http: HttpClient) {

  }

  getProducts(page: number): Observable<Product[]> {
    return this.http.get<Product[]>(`/assets/page${page}.json`);
  }

  getProduct(id: number): Observable<Product> {
    return this.http.get<Product>(`/assets/product${id}.json`);
  }

}
