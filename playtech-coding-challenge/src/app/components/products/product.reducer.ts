import {Product} from "../../models/product.model";
import {createReducer, on} from "@ngrx/store";
import {loadProducts, loadUsersSuccess} from "./product.actions";

export interface ProductState {
  products: Product[];
}

export const initialState: ProductState = {
  products: [],
}

export const productReducer = createReducer(
  initialState,
  on(loadProducts, (state) => {
    return {...state}
  }),
  on(loadUsersSuccess, (state, products) => {
    return {...state, ...products}
  }),
)
