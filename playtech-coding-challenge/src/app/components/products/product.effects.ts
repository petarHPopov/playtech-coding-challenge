import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import { map, catchError, mergeMap} from 'rxjs/operators';
import {of} from "rxjs";
import {Action} from "@ngrx/store";
import {ProductService} from "../../core/product.service";
import {loadProducts, loadUsersSuccess} from "./product.actions";

class EffectError implements Action {
  readonly type = '[Error] Effect Error DataEffects';
}

@Injectable()
export class ProductsEffects {

  constructor(private actions$: Actions, private readonly service: ProductService) {
  }

  loadProducts$ = createEffect(() => this.actions$.pipe(
    ofType(loadProducts),
    mergeMap(({page}) => this.service.getProducts(page)
      .pipe(
        map((products) => {
          return loadUsersSuccess({products});
        }),
        catchError((err) => of(new EffectError()))
      ))
  ));
}
