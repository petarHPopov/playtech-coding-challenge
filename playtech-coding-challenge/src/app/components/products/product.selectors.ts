import {createFeatureSelector, createSelector} from "@ngrx/store";
import {ProductState} from "./product.reducer";

export const getProductStore = createFeatureSelector<ProductState>('products');

export const getProducts = createSelector(getProductStore, p => p.products);

